import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;


public class ImageHandling {
    public static Image readImage(String pathname)
    {
    	Image image;
        try
        {
            image = ImageIO.read(new File(pathname));
            return image;
        }
        catch(IOException e)
        {
            System.out.println(e); 
        }
        return null;
    }

    public static Image readTransparentImage(String pathname)
    {
    	return readTransparentImage(pathname, null);
    }
    public static Image readTransparentImage(String pathname, Color alphacol)
    {
        BufferedImage source;
        try
        {
            source = ImageIO.read(new File(pathname));
        }
        catch(IOException e)
        {
            System.out.println(e);
            return null;
        }

        if (alphacol == null)
        	alphacol = new Color(source.getRGB(0, 0));

        return makeColorTransparent(source, alphacol);
    };
    
    public static Image makeColorTransparent(BufferedImage image, final Color color)
    {
        ImageFilter filter = new RGBImageFilter() {
            public int markerRGB = color.getRGB() | 0xFF000000;

            public final int filterRGB(int x, int y, int rgb) {
                if ((rgb | 0xFF000000) == markerRGB) {
                    return 0x00FFFFFF & rgb;
                } else {
                    return rgb;
                }
            }
        };

        ImageProducer ip = new FilteredImageSource(image.getSource(), filter);
        return Toolkit.getDefaultToolkit().createImage(ip);
    };
}
