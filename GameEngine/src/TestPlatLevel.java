
public class TestPlatLevel extends LevelPrecise {
    public TestPlatLevel(String iStrCfgFileName) {
        super(iStrCfgFileName);
    }

    public void loadPlayer(String iStrFileName)
    {
        player = new PlayerPrecisePlatform9(iStrFileName);

        player.MIN_SPEED_X = 1;
        player.MAX_SPEED_X = 50;
        player.MIN_SPEED_Y = 1;
        player.MAX_SPEED_Y = 50;
    }

    public void GetSensorsOrder() {}
/*    public void GetSensorsOrder()
    {
        // determine "state" :
        // - on ground / in air up / in air down
        // - move left / move right / not moving
// !!!! TO DO : "state" as global variable ???? !!!!
        int state = 0;
        if (playerSpeedX > 0)
            state = 1;
        else if (playerSpeedX < 0)
            state = 2;
        if (playerInAir)
        {
            state += 3;
            if (playerSpeedY > 0) // falling
            	state += 3;
        }
        // else assert speedY == 0 ????

// !!!! TO DO : OR compute once in table at creation ???? !!!!
//               (save memory or compute time ????)
        // compute collision sensors order
        switch(state)
        {
        case 0:
            PLAYER_SENSORS_ORDER[0] = 0;
            PLAYER_SENSORS_ORDER[1] = 2;
            PLAYER_SENSORS_ORDER[2] = 1;
            PLAYER_SENSORS_ORDER[3] = 3;
            PLAYER_SENSORS_ORDER[4] = 4;
            PLAYER_SENSORS_ORDER[5] = 5;
            PLAYER_SENSORS_ORDER[6] = 6;
            PLAYER_SENSORS_ORDER[7] = 7;
            PLAYER_SENSORS_ORDER[8] = 8;
            break;
        case 1:
            PLAYER_SENSORS_ORDER[0] = 0;
            PLAYER_SENSORS_ORDER[1] = 2;
            PLAYER_SENSORS_ORDER[2] = 1;
            PLAYER_SENSORS_ORDER[3] = 3;
            PLAYER_SENSORS_ORDER[4] = 5;
            PLAYER_SENSORS_ORDER[5] = 7;
// !!!! TO DO : needed ???? !!!!
            PLAYER_SENSORS_ORDER[6] = 4;
            PLAYER_SENSORS_ORDER[7] = 6;
            PLAYER_SENSORS_ORDER[8] = 8;
            break;
        case 2:
            PLAYER_SENSORS_ORDER[0] = 2;
            PLAYER_SENSORS_ORDER[1] = 0;
            PLAYER_SENSORS_ORDER[2] = 1;
            PLAYER_SENSORS_ORDER[3] = 4;
            PLAYER_SENSORS_ORDER[4] = 6;
            PLAYER_SENSORS_ORDER[5] = 8;
// !!!! TO DO : needed ???? !!!!
            PLAYER_SENSORS_ORDER[6] = 3;
            PLAYER_SENSORS_ORDER[7] = 5;
            PLAYER_SENSORS_ORDER[8] = 7;
            break;
        case 3:
            PLAYER_SENSORS_ORDER[0] = 7;
            PLAYER_SENSORS_ORDER[1] = 8;
            PLAYER_SENSORS_ORDER[2] = 5;
            PLAYER_SENSORS_ORDER[3] = 6;
            PLAYER_SENSORS_ORDER[4] = 3;
            PLAYER_SENSORS_ORDER[5] = 4;
            PLAYER_SENSORS_ORDER[6] = 0;
            PLAYER_SENSORS_ORDER[7] = 1;
            PLAYER_SENSORS_ORDER[8] = 2;
            break;
        case 4:
            PLAYER_SENSORS_ORDER[0] = 5;
            PLAYER_SENSORS_ORDER[1] = 7;
            PLAYER_SENSORS_ORDER[2] = 8;
            PLAYER_SENSORS_ORDER[3] = 6;
            PLAYER_SENSORS_ORDER[4] = 3;
            PLAYER_SENSORS_ORDER[5] = 0;
            PLAYER_SENSORS_ORDER[6] = 1;
            PLAYER_SENSORS_ORDER[7] = 2;
            PLAYER_SENSORS_ORDER[8] = 4;
            break;
        case 5:
            PLAYER_SENSORS_ORDER[0] = 6;
            PLAYER_SENSORS_ORDER[1] = 8;
            PLAYER_SENSORS_ORDER[2] = 7;
            PLAYER_SENSORS_ORDER[3] = 5;
            PLAYER_SENSORS_ORDER[4] = 4;
            PLAYER_SENSORS_ORDER[5] = 2;
            PLAYER_SENSORS_ORDER[6] = 1;
            PLAYER_SENSORS_ORDER[7] = 0;
            PLAYER_SENSORS_ORDER[8] = 3;
            break;
        case 6:
            PLAYER_SENSORS_ORDER[0] = 0;
            PLAYER_SENSORS_ORDER[1] = 1;
            PLAYER_SENSORS_ORDER[2] = 2;
            PLAYER_SENSORS_ORDER[3] = 3;
            PLAYER_SENSORS_ORDER[4] = 4;
            PLAYER_SENSORS_ORDER[5] = 5;
            PLAYER_SENSORS_ORDER[6] = 6;
// !!!! TO DO : needed ???? !!!!
            PLAYER_SENSORS_ORDER[7] = 7;
            PLAYER_SENSORS_ORDER[8] = 8;
            break;
        case 7:
            PLAYER_SENSORS_ORDER[0] = 0;
            PLAYER_SENSORS_ORDER[1] = 1;
            PLAYER_SENSORS_ORDER[2] = 2;
            PLAYER_SENSORS_ORDER[3] = 3;
            PLAYER_SENSORS_ORDER[4] = 4;
            PLAYER_SENSORS_ORDER[5] = 5;
            PLAYER_SENSORS_ORDER[6] = 7;
// !!!! TO DO : needed ???? !!!!
            PLAYER_SENSORS_ORDER[7] = 6;
            PLAYER_SENSORS_ORDER[8] = 8;
            break;
        case 8:
            PLAYER_SENSORS_ORDER[0] = 2;
            PLAYER_SENSORS_ORDER[1] = 1;
            PLAYER_SENSORS_ORDER[2] = 0;
            PLAYER_SENSORS_ORDER[3] = 4;
            PLAYER_SENSORS_ORDER[4] = 3;
            PLAYER_SENSORS_ORDER[5] = 6;
            PLAYER_SENSORS_ORDER[6] = 8;
// !!!! TO DO : needed ???? !!!!
            PLAYER_SENSORS_ORDER[7] = 5;
            PLAYER_SENSORS_ORDER[8] = 7;
            break;
        }
    }
*/
/*    public void makeMovement()
    {
        // !!!! TO DO : check for collisions and correct if necessary !!!!
        // ???? => update playerWorld before or after ????

// !!!! TO REMOVE !!!!
// & use function like "moveX", "moveY", ...
        playerWorldX += playerSpeedX;
/*
// !!!! TO DO : check return values ???? !!!!
//        playerWorldX += SpeedX;
        if (SpeedX > 0)
        {
            moveRight((int)SpeedX);
        }
        else if (SpeedX < 0)
        {
            moveLeft((int)-SpeedX);
        }
//        playerWorldY += SpeedY;
        if (SpeedY > 0)
        {
            moveDown((int)SpeedY);
        }
        else if (SpeedY < 0)
        {
            moveUp((int)-SpeedY);
        }
*/
/*
        GetSensorsOrder();

        // check collisions ...
/*        for (int i=0; i<NB_PLAYER_SENSORS; ++i)
        {
        	// get point details
        	// - x,y
        	// - TileX, TileY
        	// - OffsetX, OffsetY
            PLAYER_SENSORS_ORDER[i]


            // !!!! if super.layerMain._arrayMap[][] > 0 !!!!
            // !!!!   check TILES[super.layerMain._arrayMap[][]-1] H_ARRAY | V_ARRAY !!!!
        }
*/
/*    }
*/

/*
    final double PLAYER_SPEED_MAX = 6;
    final double PLAYER_ACCELERATION = 0.05; // or 0.046875 ?
    final double PLAYER_DECELERATION = 0.5;
    final double PLAYER_FRICTION = 0.05;     // or 0.046875 ?
*/
/*
    final double PLAYER_SPEED_MAX = 12;
    final double PLAYER_ACCELERATION = 0.1; // or 0.046875 ?
    final double PLAYER_DECELERATION = 1.0;
    final double PLAYER_FRICTION = 0.1;     // or 0.046875 ?
*/
}
