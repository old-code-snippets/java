public class Keyboard
{
    public Keyboard()
    {
        keyState = new boolean[256];
    }
 
    public void resetAllStates()
    {
        for(int i=0; i<keyState.length; i++)
            keyState[i] = false;
    }
   
    public boolean keyState[]; 
}