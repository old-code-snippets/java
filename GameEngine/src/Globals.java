public class Globals
{
    public static GameFramework framework;

    public static Keyboard keyboard;
    public static Mouse mouse;

    public static TemplateScreen currentScreen;
    public static TemplateScreen previousScreen;

//    public static MainScreen mainScreen;
//    public static ScrollerLevel scrollerlevel;
    public static PlatformerLevel platformerlevel;
//    public static TestPlatLevel testplatlevel;

    public static int DISPLAY_WIDTH = 800;
    public static int DISPLAY_HEIGHT = 600;
    public static String WINDOW_TITLE = "Game Framework";

//    public static SoundManager soundManager;
}