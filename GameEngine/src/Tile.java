
public class Tile {
    public Tile(int iWidth, int iHeight)
    {
        WIDTH = iWidth;
        HEIGHT = iHeight;
        H_ARRAY = new int[iHeight][2];
        V_ARRAY = new int[iWidth][2];
    }

    final int WIDTH;
    final int HEIGHT;
    int H_ARRAY[][];
    int V_ARRAY[][];
}
