import java.awt.*;
import java.awt.event.*;

public class MainScreen extends TemplateScreen
{
    public MainScreen()
    {
        LevelInfo levelinfo = new LevelInfo("config.txt");

        MAIN_LAYER_POS = levelinfo.m_iMainLayerPos;
        IS_BACKGROUND = levelinfo.m_bIsBackground;
    	if (IS_BACKGROUND)
        {
            backgroundImage = ImageHandling.readImage(levelinfo.m_strBackgroundFile);
        }

        playerSheet = ImageHandling.readTransparentImage(levelinfo.m_strPlayerSheetFile);

        LayerInfo layerInfoMain = new LayerInfo(levelinfo.m_strMainLayerFile);
        layerMain = new Layer(layerInfoMain);

        MAP_WIDTH = layerMain.MAP_WIDTH;
        MAP_HEIGHT = layerMain.MAP_HEIGHT;
        TILE_WIDTH = layerMain.TILE_WIDTH;
        TILE_HEIGHT = layerMain.TILE_HEIGHT;
        MAP_PIXEL_WIDTH = TILE_WIDTH * MAP_WIDTH;
        MAP_PIXEL_HEIGHT = TILE_HEIGHT * MAP_HEIGHT;
        // !!!! TO DO : change "TILE_WIDTH" with "PLAYER_WIDTH" ???? !!!!
        // and need SCROLL_THRESHOLD_X and SCROLL_THRESHOLD_Y ????
        SCROLL_THRESHOLD = 4 * TILE_WIDTH;

        NB_LAYERS = levelinfo.m_iNbLayers;
        loadLayers(levelinfo.m_strLayerfiles, levelinfo.m_dLayerSpeeds);


        // set max map viewable screen size
        VIEW_LIMIT_X = Math.min(bounds.width, MAP_PIXEL_WIDTH);
        VIEW_LIMIT_Y = Math.min(bounds.height, MAP_PIXEL_HEIGHT);

        // set the default scroll position
        setScrollX(levelinfo.m_iStartScrollX);
        setScrollY(levelinfo.m_iStartScrollY);

        // set the players starting position
//        playerWorldX = 3 * TILE_WIDTH;
//        playerWorldY = 3 * TILE_HEIGHT;
        playerWorldX = levelinfo.m_iPlayerStartX;
        playerWorldY = levelinfo.m_iPlayerStartY;

        updatePlayerScreenPosition();
    }

    public void loadLayers(String iLayerFiles[], double iLayerSpeeds[])
    {
        if (NB_LAYERS != 0)
        {
            layers = new Layer[NB_LAYERS];

            for(int i=0; i<NB_LAYERS; i++)
            {
                layers[i] = new Layer(new LayerInfo(iLayerFiles[i]));
                layers[i].setSpeedFactor(iLayerSpeeds[i]);
            }
        }
    }


    public boolean isClearTile(int x, int y)
    {
        return layerMain._arrayMap[x][y]==GRASS_TILE;
    }

    public boolean isValidRow(int x1, int x2, int row)
    {
        for(int i=x1; i<=x2; i++)
            if(!isClearTile(i, row))
                return false;

        return true;
    }

    public boolean isValidColumn(int y1, int y2, int column)
    {
        for(int j=y1; j<=y2; j++)
            if(!isClearTile(column, j))
                return false;

        return true;
    }


    public boolean moveLeft()
    {
        int newPosX = playerWorldX-1;

        // check out of map bounds
        if(newPosX < 0)
            return false;

        // check for blocked tiles 
        int leftColumn = newPosX / TILE_WIDTH;
        int topTile = playerWorldY / TILE_HEIGHT;
        int bottomTile = (playerWorldY+PLAYER_HEIGHT-1) / TILE_HEIGHT;

        if(isValidColumn(topTile, bottomTile, leftColumn))
        {
            playerWorldX--;
            return true;
        }
        else
            return false;
    }

    public boolean moveRight()
    {
        int newPosX = playerWorldX+1;

        // check out of map bounds
        if(newPosX+PLAYER_WIDTH > MAP_PIXEL_WIDTH)
            return false;

        // check for blocked tiles 
        int rightColumn = (newPosX+PLAYER_WIDTH-1) / TILE_WIDTH; 
        int topTile = playerWorldY / TILE_HEIGHT;
        int bottomTile = (playerWorldY+PLAYER_HEIGHT-1) / TILE_HEIGHT;

        if(isValidColumn(topTile, bottomTile, rightColumn))
        {
            playerWorldX++;
            return true;
        }
        else 
            return false;
    }

    public boolean moveUp()
    {
        int newPosY = playerWorldY-1;
 
        // check out of bounds
        if(newPosY < 0)
            return false;

        // check for blocked tiles 
        int topRow = newPosY / TILE_HEIGHT; 
        int leftTile = playerWorldX / TILE_WIDTH;
        int rightTile = (playerWorldX+PLAYER_WIDTH-1) / TILE_WIDTH;

        if(isValidRow(leftTile, rightTile, topRow))
        {
            playerWorldY--;
            return true;
        }
        else
            return false;
    }

    public boolean moveDown()
    {
        int newPosY = playerWorldY+1;

        // check out of bounds
        if(newPosY+PLAYER_HEIGHT > MAP_PIXEL_HEIGHT)
            return false;

        // check for blocked tiles 
        int bottomRow = (newPosY+PLAYER_HEIGHT-1) / TILE_HEIGHT; 
        int leftTile = playerWorldX / TILE_WIDTH;
        int rightTile = (playerWorldX+PLAYER_WIDTH-1) / TILE_WIDTH;

        if(isValidRow(leftTile, rightTile, bottomRow))
        {
            playerWorldY++;
            return true;
        }
        else
            return false;
    }

    public void updateScrollPosition()
    {
        int newPlayerScreenX = playerWorldX - scrollPosX;
        if(newPlayerScreenX < SCROLL_THRESHOLD) // check left
        {
            setScrollX(scrollPosX - (SCROLL_THRESHOLD - newPlayerScreenX));
        }
        else if(newPlayerScreenX > VIEW_LIMIT_X - SCROLL_THRESHOLD - PLAYER_WIDTH) // check right
        {
            setScrollX(scrollPosX + (newPlayerScreenX - (VIEW_LIMIT_X - SCROLL_THRESHOLD - PLAYER_WIDTH)));
        } 

        int newPlayerScreenY = playerWorldY - scrollPosY;
        if(newPlayerScreenY < SCROLL_THRESHOLD) // check top
        {
            setScrollY(scrollPosY - (SCROLL_THRESHOLD - newPlayerScreenY));
        }
        else if(newPlayerScreenY > VIEW_LIMIT_Y - SCROLL_THRESHOLD - PLAYER_HEIGHT) // check bottom
        {
            setScrollY(scrollPosY + (newPlayerScreenY - (VIEW_LIMIT_Y - SCROLL_THRESHOLD - PLAYER_HEIGHT)));
        }
    }

    public void setScrollX(int x)
    {
        scrollPosX = x;
        scrollPosX = Math.max(scrollPosX, 0);
        scrollPosX = Math.min(scrollPosX, MAP_PIXEL_WIDTH - VIEW_LIMIT_X);

        layerMain.tileOffsetX = scrollPosX % TILE_WIDTH;
        layerMain.startTileX = scrollPosX / TILE_WIDTH;
    }

    public void setScrollY(int y)
    {
        scrollPosY = y;
        scrollPosY = Math.max(scrollPosY, 0);
        scrollPosY = Math.min(scrollPosY, MAP_PIXEL_HEIGHT - VIEW_LIMIT_Y);

        layerMain.tileOffsetY = scrollPosY % TILE_HEIGHT;
        layerMain.startTileY = scrollPosY / TILE_HEIGHT;
    }

    public void setLayersScrollX()
    {
        for(int i=0; i<NB_LAYERS; ++i)
        {
    		layers[i].setScrollX(layerMain.startTileX,
                                 layerMain.tileOffsetX, TILE_WIDTH);
        }
    }

    public void setLayersScrollY()
    {
    	for(int i=0; i<NB_LAYERS; ++i)
    	{
    		layers[i].setScrollY(layerMain.startTileY,
                                 layerMain.tileOffsetY, TILE_HEIGHT);
    	}
    }


    public void updatePlayerScreenPosition()
    {
        playerScreenX = playerWorldX - scrollPosX;
        playerScreenY = playerWorldY - scrollPosY;
    }

    public void movePlayer()
    {
        int playerVectorX = (Globals.keyboard.keyState[KeyEvent.VK_LEFT]?-1:0) + (Globals.keyboard.keyState[KeyEvent.VK_RIGHT]?1:0);
        // SIDE/TOP BEGIN
//        int playerVectorY = (Globals.keyboard.keyState[KeyEvent.VK_UP]?-1:0) + (Globals.keyboard.keyState[KeyEvent.VK_DOWN]?1:0);
        // SIDE/TOP MID
        int playerVectorY = 1;

        if(playerJumpable)
        {
            if(Globals.keyboard.keyState[KeyEvent.VK_UP])
            {
                playerJump = true;
                playerJumpable = false;
                playerSpeedY = GRAVITY_SPEED;
            }
        }

        if (playerJump)
        {
        	playerVectorY = -1;
        }
        // SIDE/TOP END

        if(playerVectorX==0 && playerVectorY==0)
            return;

        // SIDE/TOP BEGIN
        // update player direction frame
        if(playerVectorY<0) playerDir = 1+playerVectorX;
        else if(playerVectorY>0) playerDir = 4+playerVectorX;
        else if(playerVectorX<0) playerDir = 6;
        else if(playerVectorX>0) playerDir = 7;
        // SIDE/TOP MID
        // !!!! TO DO : need to modify for platformer (SIDE view) !!!!
        // SIDE/TOP END

        boolean blockedMoveX = false;
        boolean blockedMoveY = false;

        // SIDE/TOP BEGIN
/*
        for(int i=0; i<playerSpeedX; i++)
        {
            if(playerVectorX<0)
                blockedMoveX = !moveLeft();
            else if(playerVectorX>0)
                blockedMoveX = !moveRight();

            if(blockedMoveX) // if can't move further
                break;
        }

        for(int i=0; i<playerSpeedY; i++)
        {
            if(playerVectorY<0)
                blockedMoveY = !moveUp();
            else if(playerVectorY>0)
                blockedMoveY = !moveDown();

            if(blockedMoveY) // if can't move further
                break;
        }
*/
        // SIDE/TOP MID

        for(int i=0; i<playerSpeedX; i++)
        {
            if(playerVectorX<0)
                blockedMoveX = !moveLeft();
            else if(playerVectorX>0)
                blockedMoveX = !moveRight();

            if(blockedMoveX) // if can't move further
                break;
        }

        for(int i=0; i<playerSpeedY; i++)
        {
            playerJumpable = false; // can't jump while "in air"

            // !!!! TO DO : use same function "moveVertical(playerVectorY)" and use return value true or false !!!!
            if(playerVectorY<0)
                blockedMoveY = !moveUp();
            else if(playerVectorY>0)
                blockedMoveY = !moveDown();

            if(blockedMoveY) // if can't move further
            {
                playerSpeedY = PLAYER_MIN_SPEED_Y;
                playerJump = false;
                if(playerVectorY>0) // hit ground
                {
                    playerJumpable = true;
                }
                break;
            }
        }
        if(!blockedMoveY)
        {
            if (playerJump)
            {
                playerSpeedY = Math.max(playerSpeedY-1, PLAYER_MIN_SPEED_Y);
                if (playerSpeedY == PLAYER_MIN_SPEED_Y) // reached top of jump
                {
                    playerJump = false;
                }
            }
            else
            {
                playerSpeedY = Math.min(playerSpeedY+1, PLAYER_MAX_SPEED_Y);
            }
        }
        // SIDE/TOP END

        updateScrollPosition();
        updatePlayerScreenPosition();
    }

    public void moveLayers()
    {
        setLayersScrollX();
        setLayersScrollY();
    }

    public void process()
    {
        movePlayer();
        moveLayers();
    }

    public void render(Graphics g)
    {
        if (IS_BACKGROUND)
        {
            g.drawImage(backgroundImage, 0, 0, null);
        }

        for(int i=0; i<MAIN_LAYER_POS; i++)
        {
            renderLayer(g, layers[i]);
        }

        renderLayer(g, layerMain);

        // draw the player
        int srcX = playerDir*PLAYER_WIDTH;
 
        g.drawImage(playerSheet,
                    playerScreenX, playerScreenY,
                    playerScreenX+PLAYER_WIDTH, playerScreenY+PLAYER_HEIGHT,
                    srcX, 0, srcX+PLAYER_WIDTH, PLAYER_HEIGHT, null);
 
        // draw player's bounding box
        g.setColor(Color.yellow);
        g.drawRect(playerScreenX, playerScreenY, PLAYER_WIDTH-1,PLAYER_HEIGHT-1);

        for(int i=MAIN_LAYER_POS; i<NB_LAYERS; i++)
        {
            renderLayer(g, layers[i]);
        }

        g.drawString("Tile Walker Demo", 10, 15);
        g.drawString("Player Speed X: "+playerSpeedX, 10, 30);
        g.drawString("Player Speed Y: "+playerSpeedY, 10, 45);
    }

    public void renderLayer(Graphics g, Layer layer)
    {
        int tileWidth = layer.TILE_WIDTH;
        int tileHeight = layer.TILE_HEIGHT;
        int mapWidth = layer.MAP_WIDTH;
        int mapHeight = layer.MAP_HEIGHT;

        int tileX = layer.startTileX;
        for(int x=-layer.tileOffsetX; x<VIEW_LIMIT_X; x+=tileWidth)
        {
            int tileY = layer.startTileY;
            for(int y=-layer.tileOffsetY; y<VIEW_LIMIT_Y; y+=tileHeight)
            {
                int srcX = layer._arrayMap[tileX%mapWidth][tileY%mapHeight];
                if (srcX > 0)
                {
                    srcX = srcX * tileWidth;
                    g.drawImage(layer._imTileSheet,
                                x, y, x+tileWidth, y+tileHeight,
                                srcX, 0, srcX+tileWidth, tileHeight, null);
                }
                tileY++;
            }
            tileX++;
        }
    }

    public void handleEvent(AWTEvent e)
    {
        switch(e.getID())
        {
            case KeyEvent.KEY_PRESSED:
            {
                KeyEvent keyEvent = (KeyEvent) e;
                int key = keyEvent.getKeyCode();

                // handle key events here...
                // SIDE/TOP BEGIN
/*
                if(key == KeyEvent.VK_PAGE_UP)
                {
                    if(playerSpeedX < PLAYER_MAX_SPEED_X)
                    {
                        playerSpeedX++;
                    }
                    if(playerSpeedY < PLAYER_MAX_SPEED_Y)
                    {
                        playerSpeedY++;
                    }
                }
                else if(key == KeyEvent.VK_PAGE_DOWN)
                {
                    if(playerSpeedX > PLAYER_MIN_SPEED_X)
                    {
                        playerSpeedX--;
                    } 
                    if(playerSpeedY > PLAYER_MIN_SPEED_Y)
                    {
                        playerSpeedY--;
                    } 
                }
*/
                // SIDE/TOP MID
                if(key == KeyEvent.VK_PAGE_UP)
                {
                    if(playerSpeedX < PLAYER_MAX_SPEED_X)
                    {
                        playerSpeedX++;
                    }
                }
                else if(key == KeyEvent.VK_PAGE_DOWN)
                {
                    if(playerSpeedX > PLAYER_MIN_SPEED_X)
                    {
                        playerSpeedX--;
                    } 
                }
                // SIDE/TOP END

                break;
            }
        }
    }


////////
    static final int GRASS_TILE = 0;
////////

    final boolean IS_BACKGROUND;
    Image backgroundImage;
// + need to handle sprites (spritesMap)
// + sprites on other layers !!!!

    final int MAIN_LAYER_POS;
    Layer layerMain;
    final int NB_LAYERS;
    Layer layers[];


    final int TILE_WIDTH;
    final int TILE_HEIGHT;

    final int MAP_WIDTH;
    final int MAP_HEIGHT;

    final int MAP_PIXEL_WIDTH;
    final int MAP_PIXEL_HEIGHT;

    final int VIEW_LIMIT_X;
    final int VIEW_LIMIT_Y;

    int scrollPosX = 0;
    int scrollPosY = 0;

    final int SCROLL_THRESHOLD;


    // Player
    // TO DO : move elsewhere ? (get from config ...) ????
    static final int PLAYER_WIDTH = 96;
    static final int PLAYER_HEIGHT = 96;

    //// SIDE/TOP - BEGIN
/*
    static final int PLAYER_MIN_SPEED_X = 1;
    static final int PLAYER_MAX_SPEED_X = 50;
    static final int PLAYER_MIN_SPEED_Y = 1;
    static final int PLAYER_MAX_SPEED_Y = 50;
*/
    //// SIDE/TOP - MID
    static final int GRAVITY_SPEED = 20;
    static final int PLAYER_MIN_SPEED_X = 1;
    static final int PLAYER_MAX_SPEED_X = 50;
    // !!!! TO DO : shouldnt be 0 ???? !!!!
    static final int PLAYER_MIN_SPEED_Y = 1;
    static final int PLAYER_MAX_SPEED_Y = 50;
    //// SIDE/TOP - END

    Image playerSheet;

    int playerWorldX;
    int playerWorldY;

    int playerScreenX;
    int playerScreenY;

    int playerDir;
    //// SIDE/TOP - BEGIN
//    int playerSpeedX = 8;
//    int playerSpeedY = 8;
    //// SIDE/TOP - MID
    int playerSpeedX = 8;
    int playerSpeedY = PLAYER_MIN_SPEED_Y;

    boolean playerJumpable = false;
    boolean playerJump = false;
    //// SIDE/TOP - END
}