import java.util.Scanner;

public class LevelInfoPrecise extends LevelInfo {
    public LevelInfoPrecise(String i_strFileName) {
        super(i_strFileName);
    }

    public void LoadMainLayerInfo(Scanner s)
    {
        m_strMainLayerFile = s.next();
        m_iMainLayerTileWidth = s.nextInt();
        m_iMainLayerTileHeight = s.nextInt();
        m_iMainLayerNbTileTypes = s.nextInt();
        if (m_iMainLayerNbTileTypes != 0)
        {
        	m_MainLayerTileTypes = new Tile[m_iMainLayerNbTileTypes];
            for(int i=0; i<m_iMainLayerNbTileTypes; ++i)
            {
            	m_MainLayerTileTypes[i] = new Tile(m_iMainLayerTileWidth, m_iMainLayerTileHeight);
                for(int j=0; j<m_iMainLayerTileHeight; ++j)
                {
                	m_MainLayerTileTypes[i].H_ARRAY[j][0] = s.nextInt();
                	m_MainLayerTileTypes[i].H_ARRAY[j][1] = s.nextInt();
                }
                @SuppressWarnings("unused")
                String tmpSeparator = s.next();
                for(int j=0; j<m_iMainLayerTileWidth; ++j)
                {
                	m_MainLayerTileTypes[i].V_ARRAY[j][0] = s.nextInt();
                   	m_MainLayerTileTypes[i].V_ARRAY[j][1] = s.nextInt();
                }
                tmpSeparator = s.next();
            }
            int iNbVisualTileTypes = s.nextInt();
        	m_MainLayerTileAssociations = new int[iNbVisualTileTypes];
            for(int i=0; i<iNbVisualTileTypes; ++i)
            {
                m_MainLayerTileAssociations[i] = s.nextInt();
            }
        } else { // no precise collisions
        	m_MainLayerTileTypes = null;
            m_MainLayerTileAssociations = null;
        }
    }

    int m_iMainLayerNbTileTypes;
    Tile m_MainLayerTileTypes[];
    int m_MainLayerTileAssociations[];
}
