
public abstract class LevelBlock extends LevelScreen {
    public LevelBlock(String i_strCfgFileName) {
        super(i_strCfgFileName, true);
    }

    public void loadPlayer(String iPlayerInfoFile)
    {
        player = new PlayerBlock(iPlayerInfoFile);

        player.MIN_SPEED_X = 1;
        player.MAX_SPEED_X = 50;
        player.MIN_SPEED_Y = 1;
        player.MAX_SPEED_Y = 50;
    }

    public boolean isClearTile(int x, int y)
    {
// !!!! TO DO : check resistancy of tile !!!!
        return (layerMain._arrayMap[x][y] == 0);
    }

    public boolean isValidRow(int x1, int x2, int row)
    {
        for(int i=x1; i<=x2; i++)
            if(!isClearTile(i, row))
                return false;

        return true;
    }

    public boolean isValidColumn(int y1, int y2, int column)
    {
        for(int j=y1; j<=y2; j++)
            if(!isClearTile(column, j))
                return false;

        return true;
    }
}
