public class Mouse
{
    public Mouse()
    {
        button = new boolean[3];
    }
 
    public void resetAllStates()
    {
        for(int i=0; i<button.length; i++)
            button[i] = false;
    }
   
    public int x, y;
    public boolean button[];
 
    public static final int LEFT_BUTTON = 0;
    public static final int RIGHT_BUTTON = 1;
    public static final int MIDDLE_BUTTON = 2;
}