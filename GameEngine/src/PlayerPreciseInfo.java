import java.util.Scanner;


public class PlayerPreciseInfo extends PlayerInfo {
    public PlayerPreciseInfo(String i_strFileName) {
        super(i_strFileName);
    }

    public void LoadInfo(Scanner s)
    {
        super.LoadInfo(s);

        iNbSensors = s.nextInt();

        if(iNbSensors != 0)
        {
            String strTmp = s.next();
            if(strTmp.equalsIgnoreCase("auto"))
            {
                bAuto = true;
            }
            else
            {
                bAuto = false;
// !!!! TO DO : read sensor points info !!!!
                //...
            }
        }
    }

    int iNbSensors;
    boolean bAuto;
}
