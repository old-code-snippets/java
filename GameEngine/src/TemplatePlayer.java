import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

public abstract class TemplatePlayer {
    public TemplatePlayer(String i_strFileName, boolean i_bUnload) {

        LoadInfo(i_strFileName);

        WIDTH = playerinfo.iWidth;
        HEIGHT = playerinfo.iHeight;
        WIDTH_DIV_2 = WIDTH / 2;
        HEIGHT_DIV_2 = HEIGHT / 2;

        ALPHA = playerinfo.bAlpha;
        ALPHA_COLOR = playerinfo.cAlphaColor;

        STANDING_SHEET_FILE = playerinfo.strStandSheetFile;
////
        WALKING_SHEET_FILE = playerinfo.strWalkSheetFile;
// !!!! TO DO : here ? or in derived classes ? !!!!
        RUNNING_SHEET_FILE = playerinfo.strRunSheetFile;
        JUMPING_SHEET_FILE = playerinfo.strJumpSheetFile;
////
        try
        {
            if(ALPHA)
                STANDING_SHEET = ImageHandling.readTransparentImage(STANDING_SHEET_FILE, ALPHA_COLOR);
            else
            	STANDING_SHEET = ImageIO.read(new File(STANDING_SHEET_FILE));

////
            if(playerinfo.bWalkable)
            {
                if(ALPHA)
                    WALKING_SHEET = ImageHandling.readTransparentImage(WALKING_SHEET_FILE, ALPHA_COLOR);
                else
                    WALKING_SHEET = ImageIO.read(new File(WALKING_SHEET_FILE));
            }
            else
                WALKING_SHEET = STANDING_SHEET;
            if(playerinfo.bRunnable)
            {
                if(ALPHA)
                    RUNNING_SHEET = ImageHandling.readTransparentImage(RUNNING_SHEET_FILE, ALPHA_COLOR);
                else
                    RUNNING_SHEET = ImageIO.read(new File(RUNNING_SHEET_FILE));
            }
            else
                RUNNING_SHEET = STANDING_SHEET;
            if(playerinfo.bJumpable)
            {
                if(ALPHA)
                    JUMPING_SHEET = ImageHandling.readTransparentImage(JUMPING_SHEET_FILE, ALPHA_COLOR);
                else
                    JUMPING_SHEET = ImageIO.read(new File(JUMPING_SHEET_FILE));
            }
            else
                JUMPING_SHEET = STANDING_SHEET;
////
        }
        catch(IOException e)
        {
            System.out.println(e); 
        }
        STANDING_NB_FRAMES = playerinfo.iStandFrames;
        STANDING_ANIM_SPEED = playerinfo.iStandAnimSpeed;
////
        if(playerinfo.bWalkable) {
            WALKING_NB_FRAMES = playerinfo.iWalkFrames;
            WALKING_ANIM_SPEED = playerinfo.iWalkAnimSpeed;
        } else {
            WALKING_NB_FRAMES = STANDING_NB_FRAMES;
            WALKING_ANIM_SPEED = STANDING_ANIM_SPEED;
        }
        if(playerinfo.bRunnable) {
            RUNNING_NB_FRAMES = playerinfo.iRunFrames;
            RUNNING_ANIM_SPEED = playerinfo.iRunAnimSpeed;
        } else {
            RUNNING_NB_FRAMES = WALKING_NB_FRAMES;
            RUNNING_ANIM_SPEED = WALKING_ANIM_SPEED;
        }
        if(playerinfo.bRunnable) {
            JUMPING_NB_FRAMES = playerinfo.iJumpFrames;
            JUMPING_ANIM_SPEED = playerinfo.iJumpAnimSpeed;
        } else {
//            JUMPING_NB_FRAMES = WALKING_NB_FRAMES;
            JUMPING_NB_FRAMES = 0;
//            JUMPING_ANIM_SPEED = WALKING_ANIM_SPEED;
            JUMPING_ANIM_SPEED = 1;
        }
////

        ICurrentSheet = STANDING_SHEET;
        iNbFrames = STANDING_NB_FRAMES;
        iCurrentAnimSpeed = STANDING_ANIM_SPEED;

        IMAGE_WIDTH = playerinfo.iImageWidth;
        IMAGE_HEIGHT = playerinfo.iImageHeight;
        IMAGE_WIDTH_DIV_2 = IMAGE_WIDTH / 2;
        IMAGE_HEIGHT_DIV_2 = IMAGE_HEIGHT / 2;

        if (i_bUnload)
            UnloadInfo();
    }

    public void LoadInfo(String i_strFileName)
    {
        playerinfo = new PlayerInfo(i_strFileName);
    }

    public void UnloadInfo()
    {
        playerinfo = null;
    }

// !!!! TO DO : return boolean ???? (could be useful) !!!!
    public boolean speedUpX(int iFactor)
    {
        if(iSpeedX < MAX_SPEED_X)
        {
            iSpeedX++;
            return true;
        }
        return false;
    }
    public boolean speedUpY(int iFactor)
    {
        if(iSpeedY < MAX_SPEED_Y)
        {
            iSpeedY++;
            return true;
        }
        return false;
    }
    public boolean speedDownX(int iFactor)
    {
        if(iSpeedX > MIN_SPEED_Y)
        {
            iSpeedX--;
            return true;
        }
        return false;
    }
    public boolean speedDownY(int iFactor)
    {
        if(iSpeedY > MIN_SPEED_Y)
        {
            iSpeedY--;
            return true;
        }
        return false;
    }

    public void setMove(int iVectorX, int iVectorY)
    {
        iMoveX = 0;
        iMoveY = 0;

//        if(iVectorX==0 && iVectorY==0)
//            return;

        // update player direction frame
        if(iVectorY < 0)
            iDir = 1 + iVectorX;
        else if(iVectorY > 0)
            iDir = 4 + iVectorX;
        else if(iVectorX < 0)
            iDir = 6;
        else if(iVectorX > 0)
            iDir = 7;

        // update player movement to do
        iMoveX = iSpeedX * iVectorX;
        iMoveY = iSpeedY * iVectorY;

        UpdateStatus();
    }

    public void UpdateStatus()
    {
        // update player status
        if ((iMoveX == 0) && (iMoveY == 0))
        {
            if(sStatus != State.STAND)
            {
                sStatus = State.STAND;
                ICurrentSheet = STANDING_SHEET;
                iNbFrames = STANDING_NB_FRAMES;
                iCurrentFrame = 0;
                iCurrentAnimSpeed = STANDING_ANIM_SPEED;
            }
        }
        else
        {
            if(sStatus != State.WALK)
            {
                sStatus = State.WALK;
                ICurrentSheet = WALKING_SHEET;
                iNbFrames = WALKING_NB_FRAMES;
                iCurrentFrame = 0;
                iCurrentAnimSpeed = WALKING_ANIM_SPEED;
            }
        }
    }

    public void move(LevelScreen level)
    {
        bBlockedMoveX = false;
        bBlockedMoveY = false;

    	if (iMoveX != 0)
    		bBlockedMoveX = !moveX(iMoveX, level);
    	if (iMoveY != 0)
    		bBlockedMoveY = !moveY(iMoveY, level);
    }

    public abstract boolean moveX(int iSpeed, LevelScreen level);
    public abstract boolean moveY(int iSpeed, LevelScreen level);

    public void render(Graphics g)
    {
        int srcX = iDir * IMAGE_WIDTH;
        int srcY = (int)(iCurrentFrame / iCurrentAnimSpeed) * IMAGE_HEIGHT;

// !!!! TO DO : handle animation frames - OK HERE or in move ???? !!!!
        if (iNbFrames > 1)
        {
            ++iCurrentFrame;
            if (iCurrentFrame == iNbFrames * iCurrentAnimSpeed)
                iCurrentFrame = 0;
        }

        g.drawImage(ICurrentSheet,
                    iScreenX - IMAGE_WIDTH_DIV_2, iScreenY - IMAGE_HEIGHT_DIV_2,
                    iScreenX + IMAGE_WIDTH_DIV_2, iScreenY + IMAGE_HEIGHT_DIV_2,
                    srcX, srcY, srcX + IMAGE_WIDTH, srcY + IMAGE_HEIGHT, null);
    }

    PlayerInfo playerinfo;

    final int WIDTH;
    final int HEIGHT;
    final int WIDTH_DIV_2;
    final int HEIGHT_DIV_2;

    final Boolean ALPHA;
    final Color ALPHA_COLOR;
    final String STANDING_SHEET_FILE;
    Image STANDING_SHEET;
    final int STANDING_NB_FRAMES;
    final int STANDING_ANIM_SPEED;

////
    final String WALKING_SHEET_FILE;
    Image WALKING_SHEET;
    final int WALKING_NB_FRAMES;
    final int WALKING_ANIM_SPEED;
// !!!! TO DO : here ? or in derived classes ? !!!!
    final String RUNNING_SHEET_FILE;
    Image RUNNING_SHEET;
    final int RUNNING_NB_FRAMES;
    final int RUNNING_ANIM_SPEED;
    final String JUMPING_SHEET_FILE;
    Image JUMPING_SHEET;
    final int JUMPING_NB_FRAMES;
    final int JUMPING_ANIM_SPEED;
////

    final int IMAGE_WIDTH;
    final int IMAGE_HEIGHT;
    final int IMAGE_WIDTH_DIV_2;
    final int IMAGE_HEIGHT_DIV_2;

    int MIN_SPEED_X = 0;
    int MAX_SPEED_X = 0;
    int MIN_SPEED_Y = 0;
    int MAX_SPEED_Y = 0;

// !!!! TO DO : OK here or in subclass ???? !!!!
// (or only STAND here, and overload in subclasses ????)
    public enum State {
        STAND, WALK, RUN, JUMP, HIT, DIE
    }
    State sStatus = State.STAND;
    Image ICurrentSheet;
    int iNbFrames = 0;
    int iCurrentFrame = 0;
    int iCurrentAnimSpeed = 1;

    int iWorldX = 0;
    int iWorldY = 0;

    int iScreenX = 0;
    int iScreenY = 0;

    int iMoveX = 0;
    int iMoveY = 0;
    boolean bBlockedMoveX = false;
    boolean bBlockedMoveY = false;

    int iSpeedX = 0;
    int iSpeedY = 0;
    int iDir = 0;
}
