import java.awt.AWTEvent;
import java.awt.event.KeyEvent;

public class PlatformerLevel extends LevelBlock {

    public PlatformerLevel(String iStrCfgFileName) {
        super(iStrCfgFileName);

        player.iSpeedX = 8;
        player.iSpeedY = player.MIN_SPEED_Y;
    }

    public void loadPlayer(String iStrFileName)
    {
        player = new PlayerBlockPlatformer(iStrFileName);

        player.MIN_SPEED_X = 1;
        player.MAX_SPEED_X = 50;
        player.MIN_SPEED_Y = 1;
        player.MAX_SPEED_Y = 50;
    }

    public void handleEvent(AWTEvent e) {
        switch(e.getID())
        {
            case KeyEvent.KEY_PRESSED:
            {
                KeyEvent keyEvent = (KeyEvent) e;
                int key = keyEvent.getKeyCode();

                // handle key events here
                if(key == KeyEvent.VK_PAGE_UP)
                {
                    player.speedUpX(1);
                }
                else if(key == KeyEvent.VK_PAGE_DOWN)
                {
                    player.speedDownX(1);
                }
                break;
            }
        }
    }

/*
    public void makeMovement()
    {
        player.move(this);
    }
*/

// !!!! TO DO : here ???? or in "mother" class ????
    static final int GRAVITY_SPEED = 20;

    boolean playerJumpable = false;
    boolean playerJump = false;
}
