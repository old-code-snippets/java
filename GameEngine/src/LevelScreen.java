import java.awt.AWTEvent;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;

public abstract class LevelScreen extends TemplateScreen
{
    public LevelScreen(String i_strCfgFileName, boolean i_bUnload)
    {
//        LevelInfo levelinfo = new LevelInfo(i_strCfgFileName);
        LoadLevelInfo(i_strCfgFileName);

        IS_BACKGROUND = levelinfo.m_bIsBackground;
        if (IS_BACKGROUND)
        {
            backgroundImage = ImageHandling.readImage(levelinfo.m_strBackgroundFile);
        }

        NB_LAYERS = levelinfo.m_iNbLayers;
        MAIN_LAYER_POS = levelinfo.m_iMainLayerPos;

// !!!! TO DO : move that to "load" (and reload at each change) ???? !!!!
        layerMain = new Layer(new LayerInfo(levelinfo.m_strMainLayerFile));
        loadLayers(levelinfo.m_strLayerfiles, levelinfo.m_dLayerSpeeds, levelinfo.m_dMovingSpeeds);

        MAP_WIDTH = layerMain.MAP_WIDTH;
        MAP_HEIGHT = layerMain.MAP_HEIGHT;
        TILE_WIDTH = layerMain.TILE_WIDTH;
        TILE_HEIGHT = layerMain.TILE_HEIGHT;
        MAP_PIXEL_WIDTH = TILE_WIDTH * MAP_WIDTH;
        MAP_PIXEL_HEIGHT = TILE_HEIGHT * MAP_HEIGHT;

//        NB_TILE_TYPES = levelinfo.m_iMainLayerNbTileTypes;

        VIEW_LIMIT_X = Math.min(bounds.width, MAP_PIXEL_WIDTH);
        VIEW_LIMIT_Y = Math.min(bounds.height, MAP_PIXEL_HEIGHT);
        // !!!! TO DO : change "TILE_WIDTH" with "PLAYER_WIDTH" ???? !!!!
        // and need SCROLL_THRESHOLD_X and SCROLL_THRESHOLD_Y ????
        // Read from file ????
        SCROLL_THRESHOLD = 4 * TILE_WIDTH;

        setScrollX(levelinfo.m_iStartScrollX);
        setScrollY(levelinfo.m_iStartScrollY);

// !!!! TO DO : add a "playerinfo" config file referred to in "config.txt" !!!!
        loadPlayer(levelinfo.m_strPlayerInfoFile);
        player.iWorldX = levelinfo.m_iPlayerStartX;
        player.iWorldY = levelinfo.m_iPlayerStartY;

        if (i_bUnload)
          UnloadLevelInfo();

        updatePlayerScreenPosition();
    }

    public void LoadLevelInfo(String i_strCfgFileName)
    {
        levelinfo = new LevelInfo(i_strCfgFileName);
    }

    public void UnloadLevelInfo()
    {
        levelinfo = null;
    }

    public void loadLayers(String iLayerFiles[], double iLayerSpeeds[], double iMovingSpeeds[][])
    {
        if (NB_LAYERS != 0)
        {
            layers = new Layer[NB_LAYERS];

            for(int i=0; i<NB_LAYERS; i++)
            {
                layers[i] = new Layer(new LayerInfo(iLayerFiles[i]));
                layers[i].setSpeedFactor(iLayerSpeeds[i]);
                layers[i].setMovingSpeeds(iMovingSpeeds[i][0], iMovingSpeeds[i][1]);
            }
        }
    }

    public abstract void loadPlayer(String iPlayerInfoFile);

    public void process()
    {
        movePlayer();
        moveLayers();
    }

    public void movePlayer()
    {
        getMovement();
//        checkMovement(); // useful ?
//        makeMovement();
        player.move(this);

        updateScrollPosition();
        updatePlayerScreenPosition();
    }

// !!!! TO DO : should move it elsewhere, to handle different input types ???? !!!!
// both different buttons and different devices (ie touchscreen)
//    public abstract void getMovement();
    public void getMovement()
    {
        int playerVectorX = (Globals.keyboard.keyState[KeyEvent.VK_LEFT]?-1:0) + (Globals.keyboard.keyState[KeyEvent.VK_RIGHT]?1:0);
        int playerVectorY = (Globals.keyboard.keyState[KeyEvent.VK_UP]?-1:0) + (Globals.keyboard.keyState[KeyEvent.VK_DOWN]?1:0);
        player.setMove(playerVectorX, playerVectorY);
    }


//    public abstract void checkMovement(); // useful ?

/*
    public void makeMovement()
    {
        player.move(this);
    }
*/

    public void updateScrollPosition()
    {
        int newPlayerScreenX = player.iWorldX - player.WIDTH_DIV_2 - scrollPosX;
        if(newPlayerScreenX < SCROLL_THRESHOLD) // check left
        {
            setScrollX(scrollPosX - (SCROLL_THRESHOLD - newPlayerScreenX));
        }
        else if(newPlayerScreenX > VIEW_LIMIT_X - SCROLL_THRESHOLD - player.WIDTH) // check right
        {
            setScrollX(scrollPosX + (newPlayerScreenX - (VIEW_LIMIT_X - SCROLL_THRESHOLD - player.WIDTH)));
        } 

        int newPlayerScreenY = player.iWorldY - player.HEIGHT_DIV_2 - scrollPosY;
        if(newPlayerScreenY < SCROLL_THRESHOLD) // check top
        {
            setScrollY(scrollPosY - (SCROLL_THRESHOLD - newPlayerScreenY));
        }
        else if(newPlayerScreenY > VIEW_LIMIT_Y - SCROLL_THRESHOLD - player.HEIGHT) // check bottom
        {
            setScrollY(scrollPosY + (newPlayerScreenY - (VIEW_LIMIT_Y - SCROLL_THRESHOLD - player.HEIGHT)));
        }
    }

    public void updatePlayerScreenPosition()
    {
        player.iScreenX = player.iWorldX - scrollPosX;
        player.iScreenY = player.iWorldY - scrollPosY;
    }

    public void moveLayers()
    {
        setLayersScrollX();
        setLayersScrollY();
    }

    public void setLayersScrollX()
    {
        for(int i=0; i<NB_LAYERS; ++i)
        {
            layers[i].setScrollX(layerMain.startTileX,
                                 layerMain.tileOffsetX, TILE_WIDTH);
        }
    }

    public void setLayersScrollY()
    {
        for(int i=0; i<NB_LAYERS; ++i)
        {
            layers[i].setScrollY(layerMain.startTileY,
                                 layerMain.tileOffsetY, TILE_HEIGHT);
        }
    }

    public void setScrollX(int x)
    {
        scrollPosX = x;
        scrollPosX = Math.max(scrollPosX, 0);
        scrollPosX = Math.min(scrollPosX, MAP_PIXEL_WIDTH - VIEW_LIMIT_X);

        layerMain.tileOffsetX = scrollPosX % TILE_WIDTH;
        layerMain.startTileX = scrollPosX / TILE_WIDTH;
    }

    public void setScrollY(int y)
    {
        scrollPosY = y;
        scrollPosY = Math.max(scrollPosY, 0);
        scrollPosY = Math.min(scrollPosY, MAP_PIXEL_HEIGHT - VIEW_LIMIT_Y);

        layerMain.tileOffsetY = scrollPosY % TILE_HEIGHT;
        layerMain.startTileY = scrollPosY / TILE_HEIGHT;
    }


    public void render(Graphics g)
    {
        if (IS_BACKGROUND)
        {
            g.drawImage(backgroundImage, 0, 0, null);
        }

        for(int i=0; i<MAIN_LAYER_POS; i++)
        {
            renderLayer(g, layers[i]);
        }

        renderLayer(g, layerMain);

        player.render(g);

        for(int i=MAIN_LAYER_POS; i<NB_LAYERS; i++)
        {
            renderLayer(g, layers[i]);
        }
    }

    public void renderLayer(Graphics g, Layer layer)
    {
        int tileWidth = layer.TILE_WIDTH;
        int tileHeight = layer.TILE_HEIGHT;
        int mapWidth = layer.MAP_WIDTH;
        int mapHeight = layer.MAP_HEIGHT;

        int tileX = layer.startTileX;
        for(int x=-layer.tileOffsetX; x<VIEW_LIMIT_X; x+=tileWidth)
        {
            int tileY = layer.startTileY;
            for(int y=-layer.tileOffsetY; y<VIEW_LIMIT_Y; y+=tileHeight)
            {
                int srcX = layer._arrayMap[tileX%mapWidth][tileY%mapHeight];
                if (srcX > 0)
                {
                    srcX = srcX * tileWidth;
                    g.drawImage(layer._imTileSheet,
                                x, y, x+tileWidth, y+tileHeight,
                                srcX, 0, srcX+tileWidth, tileHeight, null);
                }
                tileY++;
            }
            tileX++;
        }
    }

    public void handleEvent(AWTEvent e) {}



    LevelInfo levelinfo;

    final int MAP_WIDTH;
    final int MAP_HEIGHT;

    final int TILE_WIDTH;
    final int TILE_HEIGHT;

    final int MAP_PIXEL_WIDTH;
    final int MAP_PIXEL_HEIGHT;

    final int VIEW_LIMIT_X;
    final int VIEW_LIMIT_Y;

    final int SCROLL_THRESHOLD;

    final boolean IS_BACKGROUND;
    final int NB_LAYERS;
    final int MAIN_LAYER_POS;

    Image backgroundImage;
    Layer layerMain;

//    final int NB_TILE_TYPES;

    Layer layers[];

    int scrollPosX = 0;
    int scrollPosY = 0;

    TemplatePlayer player;
}
