import java.awt.Graphics;

public abstract class LevelPrecise extends LevelScreen {
    public LevelPrecise(String iStrCfgFileName) {
        super(iStrCfgFileName, false);

        LevelInfoPrecise levelinfoprecise = (LevelInfoPrecise) levelinfo;
        NB_TILE_TYPES = levelinfoprecise.m_iMainLayerNbTileTypes;
        TILE_TYPES = levelinfoprecise.m_MainLayerTileTypes;
        TILES_ASSOCIATIONS = levelinfoprecise.m_MainLayerTileAssociations;

    }

    public void LoadLevelInfo(String i_strCfgFileName)
    {
        levelinfo = new LevelInfoPrecise(i_strCfgFileName);
    }
/*
    public void loadPlayer(String iStrPlayerFile)
    {
        player = new PlayerPrecise(iStrPlayerFile, true);
    }
*/
//    public abstract void getMovement();

    public abstract void GetSensorsOrder();

    public boolean moveX(int iSpeed) {
        return false;
    }

    public boolean moveY(int iSpeed) {
        return false;
    }

    public void render(Graphics g)
    {
        super.render(g);
    }

// !!!! TO DO : move in player ???? !!!!
    final int PLAYER_MOVE_STEP_X = TILE_WIDTH;
    final int PLAYER_MOVE_STEP_Y = TILE_HEIGHT;

    final int NB_TILE_TYPES;
    final Tile TILE_TYPES[];
// !!!! TO DO : need to know "nb graphic tiles" ???? !!!!
    final int TILES_ASSOCIATIONS[];
}
