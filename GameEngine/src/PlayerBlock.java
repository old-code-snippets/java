
public class PlayerBlock extends TemplatePlayer {
    public PlayerBlock(String i_strFileName, boolean i_bUnload) {
        super(i_strFileName, i_bUnload);
    }
    public PlayerBlock(String i_strFileName) {
        super(i_strFileName, true);
    }

    public boolean moveX(int iSpeed, LevelScreen level)
    {
        LevelBlock levelBlock = (LevelBlock) level;
        if (iSpeed<0)
        {
        	iSpeed = -iSpeed;

// !!!! TO DO : instead of "-1", use "-MOVE_STEP_X"
//          so don't test for each pixel
//          (can have i.e.: MOVE_STEP_X = TILE_WIDTH)
            for(int i=0; i<iSpeed; i++)
            {
                int newPosX = iWorldX - 1;

                // check out of map bounds
// !!!! TO DO : could be removed ... ? !!!!
                if(newPosX - WIDTH_DIV_2 <0)
                    return false;

                // check for blocked tiles 
                int leftColumn = (newPosX - WIDTH_DIV_2) / levelBlock.TILE_WIDTH;
                int topTile = (iWorldY - HEIGHT_DIV_2) / levelBlock.TILE_HEIGHT;
                int bottomTile = (iWorldY + HEIGHT_DIV_2 - 1) / levelBlock.TILE_HEIGHT;

                if(levelBlock.isValidColumn(topTile, bottomTile, leftColumn))
                    iWorldX--;
                else
                    return false;
            }
        } else {
// !!!! TO DO : instead of "+1", use "+MOVE_STEP_X"
//      so don't test for each pixel
//      (can have i.e.: MOVE_STEP_X = TILE_WIDTH)
            for(int i=0; i<iSpeed; i++)
            {
                int newPosX = iWorldX + 1;

                // check out of map bounds
                if(newPosX + WIDTH_DIV_2 > levelBlock.MAP_PIXEL_WIDTH)
                    return false;

                // check for blocked tiles 
                int rightColumn = (newPosX + WIDTH_DIV_2 - 1) / levelBlock.TILE_WIDTH;
                int topTile = (iWorldY - HEIGHT_DIV_2) / levelBlock.TILE_HEIGHT;
                int bottomTile = (iWorldY + HEIGHT_DIV_2 - 1) / levelBlock.TILE_HEIGHT;

                if(levelBlock.isValidColumn(topTile, bottomTile, rightColumn))
                    iWorldX++;
                else
                    return false;
            }
        }    
        return true;
    }

    public boolean moveY(int iSpeed, LevelScreen level)
    {
    	LevelBlock levelBlock = (LevelBlock) level;
        if (iSpeed<0)
        {
        	iSpeed = -iSpeed;

// !!!! TO DO : instead of "-1", use "-MOVE_STEP_Y"
//      so don't test for each pixel
//      (can have i.e.: MOVE_STEP_Y = TILE_HEIGHT)
            for(int i=0; i<iSpeed; i++)
            {
                int newPosY = iWorldY - 1;

                // check out of bounds
                if(newPosY - HEIGHT_DIV_2 < 0)
                    return false;

                // check for blocked tiles 
                int topRow = (newPosY - HEIGHT_DIV_2) / levelBlock.TILE_HEIGHT; 
                int leftTile = (iWorldX - WIDTH_DIV_2) / levelBlock.TILE_WIDTH;
                int rightTile = (iWorldX + WIDTH_DIV_2 - 1) / levelBlock.TILE_WIDTH;

                if(levelBlock.isValidRow(leftTile, rightTile, topRow))
                    iWorldY--;
                else
                    return false;
            }
        } else {
// !!!! TO DO : instead of "+1", use "+MOVE_STEP_Y"
//      so don't test for each pixel
//      (can have i.e.: MOVE_STEP_Y = TILE_HEIGHT)
            for(int i=0; i<iSpeed; i++)
            {
                int newPosY = iWorldY + 1;

                // check out of bounds
                if(newPosY + HEIGHT_DIV_2 > levelBlock.MAP_PIXEL_HEIGHT)
                    return false;

                // check for blocked tiles 
                int bottomRow = (newPosY + HEIGHT_DIV_2 - 1) / levelBlock.TILE_HEIGHT; 
                int leftTile = (iWorldX - WIDTH_DIV_2) / levelBlock.TILE_WIDTH;
                int rightTile = (iWorldX + WIDTH_DIV_2-1) / levelBlock.TILE_WIDTH;

                if(levelBlock.isValidRow(leftTile, rightTile, bottomRow))
                    iWorldY++;
                else
                    return false;
            }
        }
        return true;
    }
}
