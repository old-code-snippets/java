import java.awt.Graphics;

public class PlayerPrecisePlatform9 extends PlayerPrecise {
    public PlayerPrecisePlatform9(String i_strFileName) {
        super(i_strFileName, 9, true);

        SENSORS_X[0] = WIDTH_DIV_2 - 1;
//?        SENSORS_Y[0] = HEIGHT_DIV_2 - 4;
// !!!! TO DO : +1 ???? !!!!
        SENSORS_Y[0] = HEIGHT_DIV_2;
        SENSORS_TYPE[0] = false; // vertical
        SENSORS_DIR[0] = false; // bottom

        SENSORS_X[1] = 0;
        SENSORS_Y[1] = HEIGHT_DIV_2;
        SENSORS_TYPE[1] = false; // vertical
        SENSORS_DIR[1] = false; // bottom

        SENSORS_X[2] = -WIDTH_DIV_2 + 1;
//?        SENSORS_Y[2] = PLAYER_HEIGHT_DIV_2 - 4;
// !!!! TO DO : +1 ???? !!!!
        SENSORS_Y[2] = HEIGHT_DIV_2 + 1;
        SENSORS_TYPE[2] = false; // vertical
        SENSORS_DIR[2] = false; // bottom

        SENSORS_X[3] = WIDTH_DIV_2;
//        SENSORS_Y[3] = HEIGHT_DIV_2 - TILE_HEIGHT + 1;
// !!!! TO DO : +1 ???? !!!!
        SENSORS_Y[3] = (HEIGHT_DIV_2 / 2) + 1;
        SENSORS_TYPE[3] = true; // horizontal
        SENSORS_DIR[3] = false; // right

        SENSORS_X[4] = -WIDTH_DIV_2;
//        SENSORS_Y[4] = HEIGHT_DIV_2 - TILE_HEIGHT + 1;
// !!!! TO DO : +1 ???? !!!!
        SENSORS_Y[4] = (HEIGHT_DIV_2 / 2) + 1;
        SENSORS_TYPE[4] = true; // horizontal
        SENSORS_DIR[4] = true; // left

        SENSORS_X[5] = WIDTH_DIV_2;
//        SENSORS_Y[5] = HEIGHT_DIV_2 - (TILE_HEIGHT * 2) + 1;
// !!!! TO DO : +1 ???? !!!!
        SENSORS_Y[5] = - HEIGHT_DIV_2 / 2;
        SENSORS_TYPE[5] = true; // horizontal
        SENSORS_DIR[5] = false; // right

        SENSORS_X[6] = -WIDTH_DIV_2;
//        SENSORS_Y[6] = HEIGHT_DIV_2 - (TILE_HEIGHT * 2) + 1;
// !!!! TO DO : +1 ???? !!!!
        SENSORS_Y[6] = - HEIGHT_DIV_2 / 2;
        SENSORS_TYPE[6] = true; // horizontal
        SENSORS_DIR[6] = true; // left

        SENSORS_X[7] = WIDTH_DIV_2 - 1;
        SENSORS_Y[7] = -HEIGHT_DIV_2;
        SENSORS_TYPE[7] = false; // vertical
        SENSORS_DIR[7] = true; // top

        SENSORS_X[8] = -WIDTH_DIV_2 + 1;
        SENSORS_Y[8] = -HEIGHT_DIV_2;
        SENSORS_TYPE[8] = false; // vertical
        SENSORS_DIR[8] = true; // top
    }

    public void GenerateSensors() {
    }

// BEGIN TO MOVE
// !!!! TO DO : move to superclass PlayerPrecisePlatform !!!!
    public void setMove(int iVectorX, int iVectorY) {
/*
        if(bJumpable)
        {
            if(iVectorY == -1)
            {
                bInAir = true;
                bJump = true;
                bJumpable = false;
                iSpeedY = JUMP_SPEED;
            }
        }

        if (bJump)
            iVectorY = -1;
//        else if (bInAir)
        else
            iVectorY = 1;
*/
//        if (!bInAir)
        {
        	// !!!! TO DO : handle brake in animation,
        	//              only if playerSpeedX >= 4.5 ????
        	// playerBrake = false; // OK? (for animation)
            //// optimize this ?
            if (iVectorX > 0)
            {
                if (dSpeedG < 0)
                {
                	// bBrake = true; // OK ?
                    dSpeedG += DECELERATION;
                }
                else if (dSpeedG < MAX_SPEED_X)
                {
                    dSpeedG += ACCELERATION;
                    if (dSpeedG > MAX_SPEED_X)
                    {
                    	dSpeedG = MAX_SPEED_X;
                    }
                }
            }
            else if (iVectorX < 0)
            {
                if (dSpeedG > 0)
                {
                	// bBrake = true; // OK ?
                    dSpeedG -= DECELERATION;
                }
                else if (dSpeedG > -MAX_SPEED_X)
                {
                    dSpeedG -= ACCELERATION;
                    if (dSpeedG < -MAX_SPEED_X)
                    {
                        dSpeedG = -MAX_SPEED_X;
                    }
                }
            }
            else
            {
            	double min = Math.min(Math.abs(dSpeedG), FRICTION);
                dSpeedG -= (min * Math.signum(dSpeedG));
            }
            ////
        }

// !!!! TO DO : different case when in air ???? !!!!
//      if (!bInAir)
        iSpeedX = (int)(dSpeedG * Math.cos(0));
        iSpeedY = - (int)(dSpeedG * Math.sin(0));

        // "hack" to be able to use super.setmove
        if(iSpeedX > 0)
            iVectorX = 1;
        else if(iSpeedX < 0)
        {
            iVectorX = -1;
            iSpeedX = -iSpeedX;
        }
        if(iSpeedY > 0)
            iVectorY = 1;
        else if(iSpeedY < 0)
        {
            iVectorY = -1;
            iSpeedY = -iSpeedY;
        }

        super.setMove(iVectorX, iVectorY);
    }

    public void UpdateStatus()
    {
        // update player status
        if ((iMoveX == 0) && (iMoveY == 0))
        {
            if(sStatus != State.STAND)
            {
                sStatus = State.STAND;
                ICurrentSheet = STANDING_SHEET;
                iNbFrames = STANDING_NB_FRAMES;
                iCurrentFrame = 0;
                iCurrentAnimSpeed = STANDING_ANIM_SPEED;
            }
        }
        else if (bInAir)
        {
            if(sStatus != State.JUMP)
            {
                sStatus = State.JUMP;
                ICurrentSheet = JUMPING_SHEET;
                iNbFrames = JUMPING_NB_FRAMES;
                iCurrentFrame = 0;
                iCurrentAnimSpeed = JUMPING_ANIM_SPEED;
            }
        }
        else if (Math.abs(iSpeedX) > 6)
        {
            if(sStatus != State.RUN)
            {
                sStatus = State.RUN;
                ICurrentSheet = RUNNING_SHEET;
                iNbFrames = RUNNING_NB_FRAMES;
                iCurrentFrame = 0;
                iCurrentAnimSpeed = RUNNING_ANIM_SPEED;
            }
        }
        else
        {
            if(sStatus != State.WALK)
            {
                sStatus = State.WALK;
                ICurrentSheet = WALKING_SHEET;
                iNbFrames = WALKING_NB_FRAMES;
                iCurrentFrame = 0;
                iCurrentAnimSpeed = WALKING_ANIM_SPEED;
            }
        }
    }

    public void move(LevelScreen level)
    {
        bBlockedMoveX = false;
        bBlockedMoveY = false;
/*
    	if (iMoveX != 0)
    		bBlockedMoveX = !moveX(iMoveX, level);
    	if (iMoveY != 0)
    		bBlockedMoveY = !moveY(iMoveY, level);
*/
        iWorldX += iMoveX;
        iWorldY += iMoveY;
        ;
    }

// !!!! TO DO : better like this, or not "abstract" in superclass ???? !!!!
    public boolean moveX(int iSpeed, LevelScreen level) { return true; }
    public boolean moveY(int iSpeed, LevelScreen level) { return true; }

// !!!! TO DO : remove (useless?) !!!!
    public void render(Graphics g)
    {
        super.render(g);
        g.drawString("PlayerPrecisePlatform", iScreenX, iScreenY+20);
    }

// !!!! TO DO : should handle the pressure of jump ... !!!!
    final int JUMP_SPEED = 20;

    boolean bInAir = true;
    boolean bJumpable = false;
    boolean bJump = false;

    double dSpeedG = 0.0;
// END TO MOVE
}
