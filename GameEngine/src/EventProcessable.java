import java.awt.*;
    
public interface EventProcessable
{
    public void handleEvent(AWTEvent e);
}