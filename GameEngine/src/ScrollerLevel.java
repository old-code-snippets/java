import java.awt.AWTEvent;
import java.awt.event.KeyEvent;

public class ScrollerLevel extends LevelBlock {

    public ScrollerLevel(String iStrCfgFileName) {
        super(iStrCfgFileName);

        player.iSpeedX = 8;
        player.iSpeedY = 8;
    }

    public void handleEvent(AWTEvent e) {
        switch(e.getID())
        {
            case KeyEvent.KEY_PRESSED:
            {
                KeyEvent keyEvent = (KeyEvent) e;
                int key = keyEvent.getKeyCode();

                // handle key events
                if(key == KeyEvent.VK_PAGE_UP)
                {
                    player.speedUpX(1);
                    player.speedUpY(1);
                }
                else if(key == KeyEvent.VK_PAGE_DOWN)
                {
                    player.speedDownX(1);
                    player.speedDownY(1);
                }
                break;
            }
        }
    }
}
