import java.awt.*;

public abstract class TemplateScreen
{
    public abstract void render(Graphics g);
 
    public void process()
    {
 
    }
 
    public void handleEvent(AWTEvent e)
    {
 
    }
 
    public void load()
    {
 
    }
 
    public void unload()
    {
 
    }
   
    public Rectangle bounds = new Rectangle(0, 0 , Globals.DISPLAY_WIDTH, Globals.DISPLAY_HEIGHT);
}