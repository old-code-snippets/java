import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Layer {
    public Layer(LayerInfo iLayerInfo)
    {
        try
        {
            if (iLayerInfo.m_bAlpha)
                _imTileSheet = ImageHandling.readTransparentImage(iLayerInfo.m_strTileSheetFile,iLayerInfo.m_cAlphaColor);
            else
                _imTileSheet = ImageIO.read(new File(iLayerInfo.m_strTileSheetFile));
        }
        catch(IOException e)
        {
            System.out.println(e); 
        }

        CFG_FILE_NAME = iLayerInfo.m_strFileName;

        MAP_WIDTH = iLayerInfo.m_iMapWidth;
        MAP_HEIGHT = iLayerInfo.m_iMapHeight;
        TILE_WIDTH = iLayerInfo.m_iTileWidth;
        TILE_HEIGHT = iLayerInfo.m_iTileHeight;
        NB_TILES = _imTileSheet.getWidth(null) / TILE_WIDTH;
        //// !!!! better to handle bi-dir tilesheets ... !!!!

        if (iLayerInfo.m_iArrayMap != null)
            _arrayMap = iLayerInfo.m_iArrayMap;
        else
            _arrayMap = new int[MAP_WIDTH][MAP_HEIGHT];

        SPEED_FACTOR = iLayerInfo.m_dSpeedFactor;
        _dMovingSpeedX = iLayerInfo.m_dMovingSpeedX;
        _dMovingSpeedY = iLayerInfo.m_dMovingSpeedY;
        _dMovingOffsetX = 0.0;
        _dMovingOffsetY = 0.0;
    }

    public void setMovingSpeeds(double iMovingSpeedX, double iMovingSpeedY)
    {
        _dMovingSpeedX = iMovingSpeedX;
        _dMovingSpeedY = iMovingSpeedY;
    }

    public void setScrollX(int iTileX, int iOffsetX, int iTileWidth)
    {
        int tileOffset = (int)(iOffsetX * SPEED_FACTOR);
        tileOffset = tileOffset * (TILE_WIDTH / iTileWidth);
        int tile = iTileX;
        double tileRealOffset = tile * SPEED_FACTOR;
        tile = (int)tileRealOffset;
        tileOffset = (int)(tileOffset + ((tileRealOffset - tile) * TILE_WIDTH));
        while(tileOffset >= TILE_WIDTH)
        {
            tileOffset = tileOffset - TILE_WIDTH;
    	    ++tile;
        }

        if(_dMovingSpeedX != 0)
        {
            _dMovingOffsetX += _dMovingSpeedX;
            tileOffset += (int)_dMovingOffsetX;
            while(tileOffset >= TILE_WIDTH)
            {
                tileOffset = tileOffset - TILE_WIDTH;
        	    ++tile;
            }
            while(tileOffset < 0)
            {
                tileOffset = tileOffset + TILE_WIDTH;
        	    --tile;
            }

            while(tile >= MAP_WIDTH)
                tile -= MAP_WIDTH;
            while(tile < 0)
                tile += MAP_WIDTH;
        }

        tileOffsetX = tileOffset;
        startTileX = tile;
    }

    public void setScrollY(int iTileY, int iOffsetY, int iTileHeight)
    {
        int tileOffset = (int)(iOffsetY * SPEED_FACTOR);
        tileOffset = tileOffset * (TILE_HEIGHT / iTileHeight);
        int tile = iTileY;
        double tileRealOffset = tile * SPEED_FACTOR;
        tile = (int)tileRealOffset;
        tileOffset = (int)(tileOffset + ((tileRealOffset - tile) * TILE_HEIGHT));
        while(tileOffset >= TILE_HEIGHT)
        {
            tileOffset = tileOffset - TILE_HEIGHT;
    	    ++tile;
        }

        if(_dMovingSpeedY != 0)
        {
            _dMovingOffsetY += _dMovingSpeedY;
            tileOffset += (int)_dMovingOffsetY;
            while(tileOffset >= TILE_HEIGHT)
            {
                tileOffset = tileOffset - TILE_HEIGHT;
        	    ++tile;
            }
            while(tileOffset < 0)
            {
                tileOffset = tileOffset + TILE_HEIGHT;
        	    --tile;
            }

            while(tile >= MAP_HEIGHT)
                tile -= MAP_HEIGHT;
            while(tile < 0)
                tile += MAP_HEIGHT;
        }

        tileOffsetY = tileOffset;
        startTileY = tile;
    }

    public void setSpeedFactor(double dSpeedFactor)
    {
        SPEED_FACTOR = dSpeedFactor;
    }

    Image _imTileSheet;

    final String CFG_FILE_NAME;
    
    final int NB_TILES;
    //// !!!! better to handle bi-dir tilesheets ... !!!!

    final int MAP_WIDTH;
    final int MAP_HEIGHT;

    final int TILE_WIDTH;
    final int TILE_HEIGHT;

//    final double SPEED_FACTOR;
    double SPEED_FACTOR;

    double _dMovingSpeedX;
    double _dMovingSpeedY;
    double _dMovingOffsetX;
    double _dMovingOffsetY;

    // TO DO : constant ? !!!!
    final int _arrayMap[][];

    int tileOffsetX = 0;
    int startTileX = 0;
    int tileOffsetY = 0;
    int startTileY = 0;
}
