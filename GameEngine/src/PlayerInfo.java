import java.awt.Color;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class PlayerInfo {
    public PlayerInfo(String i_strFileName) {

        if(!i_strFileName.equals(""))
        {
            Scanner s = null;
            try {
                s = new Scanner(new BufferedReader(
                                new FileReader(i_strFileName)));
            	LoadInfo(s);
            }
            catch(IOException e)
            {
                System.out.println(e); 
            }
            finally
            {
                s.close();
            }
        }
    }

    public void LoadInfo(Scanner s)
    {
        iWidth = s.nextInt();
        iHeight = s.nextInt();
        iImageWidth = s.nextInt();
        iImageHeight = s.nextInt();

        int alpha = s.nextInt();
        if(alpha == 1)
        {
            bAlpha = true;
            if (s.hasNextInt()) {
                int r = s.nextInt();
                int g = s.nextInt();
                int b = s.nextInt();
                cAlphaColor = new Color(r,g,b);
            } else {
                String str = s.next();
                if(str.equals("null"))
                {
                    cAlphaColor = null;
                }
            }
        } else {
            bAlpha = false;
            cAlphaColor = null;
        }

        strStandSheetFile = s.next();
        iStandFrames = s.nextInt();
        iStandAnimSpeed = s.nextInt();

        // walk anim
        strWalkSheetFile = s.next();
        if(!strWalkSheetFile.equals("-"))
        {
            bWalkable = true;
            iWalkFrames = s.nextInt();
            iWalkAnimSpeed = s.nextInt();
        }

        // run anim
        strRunSheetFile = s.next();
        if(!strRunSheetFile.equals("-"))
        {
            bRunnable = true;
            iRunFrames = s.nextInt();
            iRunAnimSpeed = s.nextInt();
        }

        // jump anim
        strJumpSheetFile = s.next();
        if(!strJumpSheetFile.equals("-"))
        {
            bJumpable = true;
            iJumpFrames = s.nextInt();
            iJumpAnimSpeed = s.nextInt();
        }

        // hit anim
        strHitSheetFile = s.next();
        if(!strHitSheetFile.equals("-"))
        {
            bHitable = true;
            iHitFrames = s.nextInt();
            iHitAnimSpeed = s.nextInt();
        }

        // die anim
        strDieSheetFile = s.next();
        if(!strDieSheetFile.equals("-"))
        {
            bDieable = true;
            iDieFrames = s.nextInt();
            iDieAnimSpeed = s.nextInt();
        }
    }

    int iWidth;
    int iHeight;
    int iImageWidth;
    int iImageHeight;
    boolean bAlpha;
    Color cAlphaColor;

    String strStandSheetFile;
// !!!! TO DO : always 8 directions ???? !!!!
// => could handle 1, 2(horiz,vert) & 4(up,down,left,right)) directions too ...
    int iStandFrames;
    int iStandAnimSpeed;

    boolean bWalkable = false;
    String strWalkSheetFile = "";
    int iWalkFrames = 0;
    int iWalkAnimSpeed;

    boolean bRunnable = false;
    String strRunSheetFile = "";
    int iRunFrames = 0;
    int iRunAnimSpeed;

    boolean bJumpable = false;
    String strJumpSheetFile = "";
    int iJumpFrames = 0;
    int iJumpAnimSpeed;

    boolean bHitable = false;
    String strHitSheetFile = "";
    int iHitFrames = 0;
    int iHitAnimSpeed;

    boolean bDieable = false;
    String strDieSheetFile = "";
    int iDieFrames = 0;
    int iDieAnimSpeed;
}
