import java.awt.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class LayerInfo {
    public LayerInfo()
    {
    }

    public LayerInfo(String strFileName)
    {
        if(!strFileName.equals(""))
        {
            m_strFileName = strFileName;

            Scanner s = null;
            try {
                s = new Scanner(new BufferedReader(
                                new FileReader(m_strFileName)));
                m_iMapWidth = s.nextInt();
                m_iMapHeight = s.nextInt();
                m_iTileWidth = s.nextInt();
                m_iTileHeight = s.nextInt();
                m_strTileSheetFile = s.next();
                int alpha = s.nextInt();
                if(alpha == 1)
                {
                    m_bAlpha = true;
                    if (s.hasNextInt()) {
                        int r = s.nextInt();
                        int g = s.nextInt();
                        int b = s.nextInt();
                        m_cAlphaColor = new Color(r,g,b);
                    } else {
                        String str = s.next();
                        if(str.equals("null"))
                        {
                            m_cAlphaColor = null;
                        }
                    }
                } else {
                    m_bAlpha = false;
                }
// !!!! TO DO : remove "if" !!!!
                if(s.hasNextInt())
                {
                    m_iArrayMap = new int[m_iMapWidth][m_iMapHeight];
                    for(int y=0; y<m_iMapHeight; ++y)
                    {
                        for(int x=0; x<m_iMapWidth; ++x)
                        {
                            m_iArrayMap[x][y] = s.nextInt();
                        }
                    }
                } else {
                    m_iArrayMap = null;
                }
            }
            catch(IOException e)
            {
                System.out.println(e); 
            }
            finally
            {
                s.close();
            }
        }
    }
    public void SetMapSize(int iMapWidth, int iMapHeight)
    {
        m_iMapWidth = iMapWidth;
        m_iMapHeight = iMapHeight;
    }
    public void SetTileSize(int iTileWidth, int iTileHeight)
    {
        m_iTileWidth = iTileWidth;
        m_iTileHeight = iTileHeight;
    }

    public void SetTileSheetFile(String iStrTileSheetFile)
    {
        m_strTileSheetFile = iStrTileSheetFile;
    }

    public void SetAlpha(Boolean iAlpha)
    {
        m_bAlpha = iAlpha;
    }
    public void SetAlphaColor(Color iAlphaColor)
    {
        m_bAlpha = true;
        m_cAlphaColor = iAlphaColor;
    }

    public void SetSpeedFactor(double dSpeedFactor)
    {
    	if (dSpeedFactor > 0.0)
    	{
            m_dSpeedFactor = dSpeedFactor;
    	}
    }
    public void SetScrollSpeeds(double iScrollSpeedX, double iScrollSpeedY)
    {
        m_dMovingSpeedX = iScrollSpeedX;
        m_dMovingSpeedY = iScrollSpeedY;
    }


    String m_strFileName = "";

    // TO DO : def values ????
    int m_iMapWidth = 1;
    int m_iMapHeight = 1;

    int m_iArrayMap[][];

    int m_iTileWidth = 32;
    int m_iTileHeight = 32;

	String m_strTileSheetFile;

    Boolean m_bAlpha = false;
    Color m_cAlphaColor = null;

    double m_dSpeedFactor = 1.0;
    double m_dMovingSpeedX = 0;
    double m_dMovingSpeedY = 0;

    ////
    int m_iNbTiles = 0;
/*
    static final int TILE_TILE1 = 0;
    static final int TILE_TILE2 = 1;
    static final int TILE_TILE3 = 2;
*/
    ////
}
