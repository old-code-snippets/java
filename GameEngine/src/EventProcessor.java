import java.awt.*;
import java.util.*;
    
public class EventProcessor
{
    public EventProcessor(EventProcessable handler)
    {
        eventList = new LinkedList<AWTEvent>();
        this.handler = handler;
    }
    
    public void addEvent(AWTEvent event)
    {   
        synchronized(eventList)
        {
            eventList.add(event);
        }
    }
  
    public void processEventList()
    {
        AWTEvent event;
   
        while(eventList.size() > 0)
        {
            synchronized(eventList)
            {
                event = (AWTEvent) eventList.removeFirst();
            }
   
            handler.handleEvent(event);
        }
    }
    
    private LinkedList<AWTEvent> eventList;
    private EventProcessable handler;
}