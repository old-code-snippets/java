import java.awt.Color;
import java.awt.Graphics;

public abstract class PlayerPrecise extends TemplatePlayer {
//    public PlayerPrecise(String i_strFileName, boolean i_bUnload) {
    public PlayerPrecise(String i_strFileName, int i_iNbSensors, boolean i_bUnload) {
        super(i_strFileName, false);

        MIN_SPEED_X = 0;
        MAX_SPEED_X = 12;
        MIN_SPEED_Y = 0;
        MAX_SPEED_Y = 12;

        ACCELERATION = 0.1; // or 0.046875 ?
        DECELERATION = 1.0;
        FRICTION = 0.1;     // or 0.046875 ?

//        PlayerPreciseInfo playerpreciseinfo = (PlayerPreciseInfo) playerinfo;
//        SENSORS_NB = playerpreciseinfo.iNbSensors;
        SENSORS_NB = i_iNbSensors;
        if(SENSORS_NB != 0)
        {
// !!!! TO DO : add "grab" handling
            SENSORS_X = new int[SENSORS_NB];
            SENSORS_Y = new int[SENSORS_NB];
            SENSORS_TYPE = new boolean[SENSORS_NB];
            SENSORS_DIR = new boolean[SENSORS_NB];
            SENSORS_ORDER = new int[SENSORS_NB];

//            GenerateSensors(playerpreciseinfo);
            GenerateSensors();
        }

        if(i_bUnload)
            super.UnloadInfo();
    }

    public void LoadInfo(String i_strFileName)
    {
//        playerinfo = new PlayerPreciseInfo(i_strFileName);
        playerinfo = new PlayerInfo(i_strFileName);
    }

//    public abstract void GenerateSensors(PlayerPreciseInfo playerpreciseinfo);
    public abstract void GenerateSensors();

    public boolean moveX(int iSpeed, LevelScreen level) {
        return false;
    }

    public boolean moveY(int iSpeed, LevelScreen level) {
        return false;
    }

    public void render(Graphics g)
    {
        super.render(g);

        for (int i=0; i<SENSORS_NB; ++i)
        {
            g.setColor(new Color(255,0,0));
            if(SENSORS_TYPE[i]) // horizontal
                g.drawRect(iScreenX + SENSORS_X[i], iScreenY + SENSORS_Y[i] - 1, 1, 3);
            else //vertical
                g.drawRect(iScreenX + SENSORS_X[i]-1, iScreenY + SENSORS_Y[i], 3, 1);
        }
        g.drawString("PlayerPrecise", iScreenX, iScreenY);
    }

    final double ACCELERATION;
    final double DECELERATION;
    final double FRICTION;

/*
//    final int SENSORS_NB = 11;
    final int SENSORS_NB = 9;
    final int SENSORS_X[];
    final int SENSORS_Y[];
    final boolean SENSORS_TYPE[]; // 0: horiz, 1: vert
    final boolean SENSORS_DIR[]; // 0: top or left, 1: bottom or right
*/
//    final int SENSORS_NB = 11;
    int SENSORS_NB = 0;
//!!!! TO DO : use 2 different arrays (_X & _Y) or only a [2][] table ???? !!!!
    int SENSORS_X[];
    int SENSORS_Y[];
    boolean SENSORS_TYPE[]; // 0: horiz, 1: vert
    boolean SENSORS_DIR[]; // 0: top or left, 1: bottom or right
    int SENSORS_ORDER[];
}
