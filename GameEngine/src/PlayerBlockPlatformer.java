
public class PlayerBlockPlatformer extends PlayerBlock {
    public PlayerBlockPlatformer(String i_strFileName, boolean i_bUnload) {
        super(i_strFileName, i_bUnload);
    }
    public PlayerBlockPlatformer(String i_strFileName) {
        super(i_strFileName, true);
    }

    public void setMove(int iVectorX, int iVectorY)
    {
        if(bJumpable)
        {
            if(iVectorY == -1)
            {
                bInAir = true;
                bJump = true;
                bJumpable = false;
                iSpeedY = JUMP_SPEED;
            }
        }

        if (bJump)
            iVectorY = -1;
//        else if (bInAir)
        else
            iVectorY = 1;

// !!!! TO DO : should be able to handle "jumps in jumps"
//        if (iVectorY != 0)
            bJumpable = false; // can't jump while "in air"

        super.setMove(iVectorX, iVectorY);
    }

    public void UpdateStatus()
    {
        // update player status
        if ((iMoveX == 0) && (iMoveY == 0))
        {
            if(sStatus != State.STAND)
            {
                sStatus = State.STAND;
                ICurrentSheet = STANDING_SHEET;
                iNbFrames = STANDING_NB_FRAMES;
                iCurrentFrame = 0;
                iCurrentAnimSpeed = STANDING_ANIM_SPEED;
            }
        }
        else if (bInAir)
        {
            if(sStatus != State.JUMP)
            {
                sStatus = State.JUMP;
                ICurrentSheet = JUMPING_SHEET;
                iNbFrames = JUMPING_NB_FRAMES;
                iCurrentFrame = 0;
                iCurrentAnimSpeed = JUMPING_ANIM_SPEED;
            }
        }
        else
        {
            if(sStatus != State.WALK)
            {
                sStatus = State.WALK;
                ICurrentSheet = WALKING_SHEET;
                iNbFrames = WALKING_NB_FRAMES;
                iCurrentFrame = 0;
                iCurrentAnimSpeed = WALKING_ANIM_SPEED;
            }
        }
    }

    public void move(LevelScreen level)
    {
        super.move(level);

// !!!! TO DO : OK here ???? !!!!
        bInAir = true;
        if(bBlockedMoveY) // if can't move further
        {
            iSpeedY = MIN_SPEED_Y;
            bJump = false;
            if(iMoveY>0) // hit ground
            {
                bJumpable = true;
                bInAir = false;
            }
        }
        else
        {
            if (bJump)
            {
                iSpeedY = Math.max(iSpeedY - 1, MIN_SPEED_Y);
                if (iSpeedY == MIN_SPEED_Y) // reached top of jump
                    bJump = false;
            }
            else
            {
                iSpeedY = Math.min(iSpeedY + 1, MAX_SPEED_Y);
            }
        }
    }

// !!!! TO DO : should handle the pressure of jump ... !!!!
    final int JUMP_SPEED = 20;

    boolean bInAir = true;
    boolean bJumpable = false;
    boolean bJump = false;
}
