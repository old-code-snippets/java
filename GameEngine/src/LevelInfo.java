import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class LevelInfo {
    public LevelInfo(String i_strFileName)
    {
        if(!i_strFileName.equals(""))
        {
            Scanner s = null;
            try {
                s = new Scanner(new BufferedReader(
                                new FileReader(i_strFileName)));
                LoadLevelInfo(s);
            }
            catch(IOException e)
            {
                System.out.println(e); 
            }
            finally
            {
                s.close();
            }

            try {
                s = new Scanner(new BufferedReader(
                                new FileReader(m_strMainLayerInfoFile)));
                LoadMainLayerInfo(s);
            }
            catch(IOException e)
            {
                System.out.println(e); 
            }
            finally
            {
                s.close();
            }
        }
    }

    public void LoadLevelInfo(Scanner s)
    {
        if(s.nextInt() == 1)
        {
            m_bIsBackground = true;
            m_strBackgroundFile = s.next();
        }
        m_iMainLayerPos = s.nextInt();
        m_strMainLayerInfoFile = s.next();
        m_iNbLayers = s.nextInt();
        m_strLayerfiles = new String[m_iNbLayers];
        m_dLayerSpeeds = new double[m_iNbLayers];
        m_dMovingSpeeds = new double[m_iNbLayers][2];
        for(int i=0; i<m_iNbLayers; ++i)
        {
            m_strLayerfiles[i] = s.next();
            m_dLayerSpeeds[i] = s.nextDouble();
            m_dMovingSpeeds[i][0] = s.nextDouble();
            m_dMovingSpeeds[i][1] = s.nextDouble();
        }

        m_iStartScrollX = s.nextInt();
        m_iStartScrollY = s.nextInt();

/*
// !!!! TO DO : move to PlayerInfo !!!!
        m_iPlayerWidth = s.nextInt();
        m_iPlayerHeight = s.nextInt();
        m_iPlayerImageWidth = s.nextInt();
        m_iPlayerImageHeight = s.nextInt();
*/
        m_iPlayerStartX = s.nextInt();
        m_iPlayerStartY = s.nextInt();
        m_strPlayerInfoFile = s.next();
    }

    public void LoadMainLayerInfo(Scanner s)
    {
        m_strMainLayerFile = s.next();
        m_iMainLayerTileWidth = s.nextInt();
        m_iMainLayerTileHeight = s.nextInt();
//        m_iMainLayerNbTileTypes = s.nextInt();
    }

    boolean m_bIsBackground = false;
    String m_strBackgroundFile = "";

    int m_iMainLayerPos = 0;
    String m_strMainLayerFile;
// !!!! TO DO : could be a local variable ... !!!!
    String m_strMainLayerInfoFile;
    int m_iMainLayerTileWidth;
    int m_iMainLayerTileHeight;
//    int m_iMainLayerNbTileTypes;

    int m_iNbLayers = 0;
    String m_strLayerfiles[];
    double m_dLayerSpeeds[];
    double m_dMovingSpeeds[][];

    int m_iStartScrollX = 0;
    int m_iStartScrollY = 0;

    int m_iPlayerStartX;
    int m_iPlayerStartY;
    String m_strPlayerInfoFile = "";
}
