
// movable interface
/*
interface Moveable {
  public abstract void setPosition (int x, int y);
  public abstract void setVelocity (int x, int y);
  public abstract void updatePosition ();
}
*/
interface Moveable {
  void setPosition (int x, int y);
  void setVelocity (int x, int y);
  void updatePosition ();
}

