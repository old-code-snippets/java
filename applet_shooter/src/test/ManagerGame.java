import java.applet.*;
import java.awt.*;

// ManagerGame Class
public class ManagerGame extends Applet implements Runnable {

  // animation variables
  static final int REFRESH_RATE = 80; // in ms
  Thread animation;
  Graphics offscreen;
  Image image;

  int width, height; // applet dimensions

  String string1 = "string1";
  String string2 = "string2";


  // Init
  public void init() {
    super.init();

    showStatus ("Loading Images -- WAIT!");

    setBackground (Color.black); // applet background

    width = getBounds().width; // set applet dimensions
    height = getBounds().height;

    image = createImage (width, height); // make offscreen buffer
    offscreen = image.getGraphics();

  }
/*
  // handle mouse events
  public boolean mouseMove (Event e, int x, int y) {
    showStatus("MOVE");
    return true;
  }

  public boolean mouseDrag (Event e, int x, int y) {
    showStatus("MOVE DRAG");
    return true;
  }
  public boolean mouseDown (Event e, int x, int y) {
    showStatus("MOUSE DOWN");
    return true;
  }
*/
  // start the Video Game Loop
  public void start() {
    showStatus ("Starting Game!");
    animation = new Thread (this);
    if (animation != null) {
      animation.start();
    }
  }

  // override update so it doesn't erase screen
  public void update (Graphics g) {
    paint(g);
  }

  // paint the applet depending on mode of game
  public void paint (Graphics g) {

      offscreen.setColor (Color.black);
      offscreen.fillRect (0, 0, width, height); // clear buffer

      // draw status info
      offscreen.setColor (Color.cyan);
      offscreen.drawString (string1, width - 113, 13);
      offscreen.drawString (string2, width - 113, 27);

      g.drawImage (image, 0, 0, this);
  }

  // the Video Game Loop
  public void run() {
    while (animation != null) {
      repaint();

      Thread.currentThread().yield();
      try {
        Thread.sleep (REFRESH_RATE);
      } catch (Exception exc) { };
    }
  }

  // stop animation
  public void stop() {
    showStatus ("Game Stopped");
    if (animation != null) {
      animation = null;
    }
  }
}

