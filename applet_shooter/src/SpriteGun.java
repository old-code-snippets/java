import java.applet.Applet;
import java.awt.Image;

// SpriteGun Class
class SpriteGun extends SpriteBitmap implements Moveable, Intersect {

  protected ManagerGun gm; // pointer to manager class

  public SpriteGun (Image i, Applet a, ManagerGun gm) {
    super (i,a);
    this.gm = gm;
  }

  // the following methods implement Moveable:
  public void setPosition (int x, int y) {
    locx = x;
    locy = y;
  }
  public void setVelocity (int x, int y) {
  }
  public void updatePosition() {
  }

  // the following methods implement Intersect:
  // compare bounding boxes
  public boolean intersect (int x1, int y1, int x2, int y2) {
    return visible && (x2 >= locx) && (locx+width >= x1)
                   && (y2 >= locy) && (locy+height >= y1);
  }

  // tell manager to display the hit
  public void hit() {
    gm.handleHit(); // notify manager of hit
  }
}

