import java.awt.Color;

// SpriteMissile Class
class SpriteMissile extends SpriteRect {
  protected int vy; // velocity in y coordinate
  protected int start_y; // starting y coord
  protected int stop_y; // stop at y coord
  Intersect target[];

  public SpriteMissile (int w,int h,Color c,int vy,
                        int start_y,int stop_y,
                        Intersect target[]) {
    super(w,h,c);

    setFill (true); // fill rect sprite
    this.vy = vy; // initialize speed
    this.start_y = start_y; // initialize starting point
    this.stop_y = stop_y; // initialize stopping point
    this.target = target; // initialize targets
    suspend();
  }

  // start the missile at the given x coordinate
  public void init (int x) {
    locx = x;
    locy = start_y;
    restore();
  }

  public void update() {
    if (active) {
      // move missile
      locy += vy;
      if (locy < stop_y) {
        suspend();
      }
      // else if missile hits target, suspend it
      else {
        for (int i=0; i<target.length; i++) {
          if (target[i].intersect (locx,locy,
                                   locx+width,locy+height)) {
            target[i].hit(); // tell target it's been hit
            suspend();
            break;
          }
        }
      }
    }
  }
}

