import java.applet.Applet;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Color;

// ManagerGun Class
public class ManagerGun {
  private SpriteGun gun; // your gun
  private int gun_width; // width of gun
  private int gun_height;

  private SpriteMissile missile; // missile

  private int min_x, max_x; // min and max x coords

  // for gun movement
  private int gun_min_x, gun_max_x;
  private int mis_min_x, mis_max_x;
  private int gun_y;

  private boolean displayHit;
  private int energy;
  private int maxEnergy;
  private int energyDec;

  private ManagerGame game; // ptr to game manager

  static int width, height; // applet dimensions
  static final int ENERGY_PER_HIT = 5; // energy used per hit
  static final int MISSILE_WIDTH = 3;
  static final int MISSILE_HEIGHT = 27;
  static final int MISSILE_SPEED = -27; // missile flies upward
  static final Color MISSILE_COLOR = Color.red;

  public ManagerGun (int maxEnergy, int energyDec, int width, int height,
                     Image gunImage, Intersect target[], Applet a) {
    this.maxEnergy = maxEnergy;
    this.energyDec = energyDec;

    this.width = width;
    this.height = height;

    gun = new SpriteGun (gunImage, a, this);
    gun_width = gunImage.getWidth(a)/2;
    gun_height = gunImage.getHeight(a);
    gun_y = height - gun_height;
    min_x = gun_width;
    max_x = width - gun_width;
    gun_min_x = 0;
    gun_max_x = width - 2*gun_width;
    mis_min_x = min_x-2;
    mis_max_x = max_x-2;
    gun.setPosition (width/2 - gun_width, gun_y);

    missile = new SpriteMissile (MISSILE_WIDTH, MISSILE_HEIGHT,
                                 MISSILE_COLOR, MISSILE_SPEED,
                                 height - gun_height + 13,
                                 0, target);

    game = (ManagerGame)a; // set ptr to ManagerGame
  }

  // set parameters for the new game
  public void newGame() {
    gun.setPosition (width/2 - gun_width, gun_y);
    gun.restore();
    displayHit = false;
    energy = maxEnergy;
  }

  // move gun to the given x coordinate
  public void moveGun (int x) {
    if (x <= min_x) {
      gun.setPosition (gun_min_x, gun_y);
    }
    else if (x >= max_x) {
      gun.setPosition (gun_max_x, gun_y);
    }
    else {
      gun.setPosition (x-gun_width, gun_y);
    }
  }

  // fire missile from given x coordinate
  public void fireMissile (int x) {
    if (!missile.isActive()) { // if missile sprite
      // isn't active
      if (x <= min_x) {
        missile.init (mis_min_x);
      }
      else if (x >= max_x) {
        missile.init (mis_max_x);
      }
      else {
        missile.init (x-1); // initialize missile
      }
    }
  }

  // update all the parameters associated with the
  // gun. In this case, only the missile needs to move
  // automatically.
  public void update() {
    missile.update();
  }

  // paint all sprites associated with gun
  // also paint status display for amount of energy left
  String energyString = "Energy";
  public void paint (Graphics g) {
    // if gun is hit, flash a red rectangle
    // instead of painting gun
    if (displayHit) {
      g.setColor (Color.red);
      g.fillRect (0, gun_y, width, gun_height);
      displayHit = false;
    }
    else {
      gun.paint (g);
    }
    missile.paint (g);

    // display energy left
    g.setColor (Color.red);
    g.drawString (energyString, 3, 13);
    g.fillRect (0, 17, energy, 10);
  }

  // accessor function for gun
  public SpriteGun getGun() {
    return gun;
  }

  // get the y-coordinate of the gun
  public int getGunY() {
    return gun_y;
  }

  // handles a hit from an alien
  public void handleHit() {
    displayHit = true; // set display hit flag
    energy -= energyDec; // update energy
    if (energy <= 0) { // game over if energy <= 0
      game.gameOver(); // notify game manager
      gun.suspend(); // turn off sprites
      missile.suspend();
    }
  }

  // set the amount of energy lost per hit (energy decrement)
  public void setEnergyDec (int dec) {
    energyDec = dec;
  }
}

